***Address Book in Laravel 5.0 using MySQL as the DB***

This application has below user functionality

1. User Registration.
2. User Login.
3. Change User Password.

Below are the functionalies of Address Book

1. List user addresses.
2. Add user address.
3. Edit user address.
4. Delete user address.

Fields for each Address Book entry:

1. Address Book Title ( Home, Work, etc.)
2. Contact Person Name
3. Contact Person Number
4. Address Line 1
5. Address Line 2
6. Address Line 3
7. Pincode
8. City
9. State
10. Country
11. Default Address For

There can be 2 default addresses, a ‘default from’ address and a ‘default to’ address.

This application also provides RESTful API to manage the address book.
To access REST API you need to first request access tocken

```
	POST	/address_book/oauth/token
```

Access token recieved in the response (access_token) needs to be passed as Authorization header for other requests.
```
Authorization: Bearer {{token}}
```

Below is the API request one can make

```
	GET		/api/address		- list addresses added by user
	POST	/api/address		- adds address for the user
	PUT		/api/address/{id}	- updates the address for the user
	GET		/api/address/{id}	- gets the specific address for the user
	DELETE	/api/address/{id}	- deletes the specific address for the user
```

Below is the link of postman collection which you can use to test the REST api

[Postman] (https://www.getpostman.com/collections/ad477d5b2f250fd8d523)

