@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="links">
                        <a href="{{ URL::to('changePassword') }}">Manage Profile</a>
                        <br/>
                        <a href="{{ URL::to('address') }}">Manage Address Book</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
