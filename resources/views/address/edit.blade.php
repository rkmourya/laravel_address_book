@extends('layouts.app')

@section('content')
<div class="container">
    <h1>Update Address</h1> <a href="{{ URL::to('/address') }}"> Back to Addresses</a>
    <hr>
    @if (Session::has('message'))
        <div class="alert {{ Session::get('alert_class') }}">{{ Session::get('message') }}</div>
    @endif
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <form action="{{url('address', [$address->id])}}" method="post" class="form-horizontal" role="form">
        <input type="hidden" name="_method" value="PUT">
        {{ csrf_field() }}
        <div class="form-group">
            <div class="col-sm-4">
                <label for="title" class="control-label">Address Book Title</label>
            </div>
            <div class="col-sm-6">
                <input type="text" value="{{$address->title}}" class="form-control" id="title" name="title">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-4">
                <label for="person_name" class="control-label">Contact Person Name</label>
            </div>
            <div class="col-sm-6">
                <input type="text" value="{{$address->person_name}}" class="form-control" id="person_name" name="person_name">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-4">
                <label for="person_number" class="control-label">Contact Person Number</label>
            </div>
            <div class="col-sm-6">
                <input type="text" value="{{$address->person_number}}" class="form-control" id="person_number" name="person_number">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-4">
                <label for="address_line1" class="control-label">Address Line 1</label>
            </div>
            <div class="col-sm-6">
                <input type="text" value="{{$address->address_line1}}" class="form-control" id="address_line1" name="address_line1">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-4">
                <label for="address_line2" class="control-label">Address Line 2</label>
            </div>
            <div class="col-sm-6">
                <input type="text" value="{{$address->address_line2}}" class="form-control" id="address_line2" name="address_line2">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-4">
                <label for="address_line3" class="control-label">Address Line 3</label>
            </div>
            <div class="col-sm-6">
                <input type="text" value="{{$address->address_line3}}" class="form-control" id="address_line3" name="address_line3">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-4">
                <label for="pincode" class="control-label">Pincode</label>
            </div>
            <div class="col-sm-6">
                <input type="text" value="{{$address->pincode}}" class="form-control" id="pincode" name="pincode">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-4">
                <label for="city" class="control-label">City</label>
            </div>
            <div class="col-sm-6">
                <input type="text" value="{{$address->city}}" class="form-control" id="city" name="city">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-4">
                <label for="state" class="control-label">State</label>
            </div>
            <div class="col-sm-6">
                <input type="text" value="{{$address->state}}" class="form-control" id="state" name="state">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-4">
                <label for="country" class="control-label">Country</label>
            </div>
            <div class="col-sm-6">
                <input type="text" value="{{$address->country}}" class="form-control" id="country" name="country">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-4">
                <label for="default_type" class="control-label">Use Default For</label>
            </div>
            <div class="col-sm-6">
                <select class="form-control" id="default_type" name="default_type">
                    <option value="">None</option>
                    @if ($address->default_type == "from")
                          <option value="from" selected>From</option>
                    @else
                          <option value="from">From</option>
                    @endif
                    @if ($address->default_type == "to")
                          <option value="to" selected>To</option>
                    @else
                          <option value="to">To</option>
                    @endif
                </select>
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
@endsection