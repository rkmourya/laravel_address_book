@extends('layouts.app')

@section('content')
<div class="container">
    @if (Session::has('message'))
        <div class="alert {{ Session::get('alert_class') }}">{{ Session::get('message') }}</div>
    @endif
    <h3>Your Addresses</h3>
    <a href="{{ URL::to('address/create') }}">Add New Address</a>
    <table class="table">
        <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Title</th>
                <th scope="col">Contact Name</th>
                <th scope="col">Contact Number</th>
                <th scope="col">Address</th>
                <th scope="col">City</th>
                <th scope="col">Country</th>
                <th scope="col">Default For</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($addresses as $address)
            <tr>
                <th scope="row">{{$address->id}}</th>
                <td>{{$address->title}}</td>
                <td>{{$address->person_name}}</td>
                <td>{{$address->person_number}}</td>
                <td>{{$address->address_line1}} {{$address->address_line2}} {{$address->address_line3}}</td>
                <td>{{$address->city}}</td>
                <td>{{$address->country}}</td>
                <td>{{$address->default_type}}</td>
                <td>
                    <div class="btn-group" role="group" aria-label="Basic example">
                        <a href="{{ URL::to('address/' . $address->id . '/edit') }}">
                        <button type="button" class="btn btn-warning">Edit</button>
                        </a>&nbsp;
                        <form action="{{url('address', [$address->id])}}" method="POST">
                            <input type="hidden" name="_method" value="DELETE">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="submit" class="btn btn-danger" value="Delete"/>
                        </form>
                    </div>
                </td>
              </tr>
          @endforeach
        </tbody>
    </table>
</div>
@endsection