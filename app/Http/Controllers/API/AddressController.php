<?php
namespace App\Http\Controllers\API;


use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Address;
use App\User;
use Auth;
use Validator;

class AddressController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = Auth::user()->id;
        $address = Address::findMany($user_id);
        $addresses = User::with('address')->find($user_id)->address;
        return $this->sendResponse($addresses->toArray(), 'Address retrieved successfully.');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('address.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validate
        $request->validate([
            'title' => 'required|min:3',
            'person_name' => 'required|min:3',
            'person_number' => 'required|min:10|numeric',
            'address_line1' => 'required|min:5',
            'address_line2' => 'min:3',
            'address_line3' => 'min:3',
            'pincode' => 'required|min:5',
            'city' => 'required|min:2',
            'state' => 'required|min:2',
            'country' => 'required|min:2'
        ]);

        $user_id = Auth::user()->id;

        #If use wants to set this as default address, clear already present default_type
        if ( !empty($request->default_type) ) {
            Address::clearDefault($user_id, $request->default_type);
        }

        $address = Address::create([
            'user_id' => $user_id,
            'title' => $request->title,
            'person_name' => $request->person_name,
            'person_number' => $request->person_number,
            'address_line1' => $request->address_line1,
            'address_line2' => $request->address_line2,
            'address_line3' => $request->address_line3,
            'pincode' => $request->pincode,
            'city' => $request->city,
            'state' => $request->state,
            'country' => $request->country,
            'default_type' => $request->default_type,
        ]);
        if( $address->exists ) {
            return $this->sendResponse($address->toArray(), 'Address saved successfully.');
        } else {
            return $this->sendError('Failed to save address.', [], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Address  $address
     * @return \Illuminate\Http\Response
     */
    public function show(Address $address)
    {
        $user_id = Auth::user()->id;
        if ( $address->user_id != $user_id ) {
            return $this->sendError('You are not authorized to view this address', [], 403);
        }
        return $this->sendResponse($address->toArray(), 'Address retrieved successfully.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Address  $address
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Address $address)
    {
        $user_id = Auth::user()->id;
        if ( $address->user_id != $user_id ) {
            return $this->sendError('You are not authorized to update this address', [], 403);
        }

        //Validate
        $request->validate([
            'title' => 'min:3',
            'person_name' => 'min:3',
            'person_number' => 'min:10|numeric',
            'address_line1' => 'min:5',
            'address_line2' => 'min:3',
            'address_line3' => 'min:3',
            'pincode' => 'min:5',
            'city' => 'min:2',
            'state' => 'min:2',
            'country' => 'min:2'
        ]);

        #If use wants to set this as default address, clear already present default_type
        if ( !empty($request->default_type) ) {
            Address::clearDefault($user_id, $request->default_type);
        }

        $address->title = $request->title;
        $address->person_name = $request->person_name;
        $address->person_number = $request->person_number;
        $address->address_line1 = $request->address_line1;
        $address->address_line2 = $request->address_line2;
        $address->address_line3 = $request->address_line3;
        $address->pincode = $request->pincode;
        $address->city = $request->city;
        $address->state = $request->state;
        $address->country = $request->country;
        $address->default_type = $request->default_type;
        $saved = $address->save();

        if ( !$saved ) {
            return $this->sendError('Failed to update the address.', [], 500);
        }

        return $this->sendResponse($address->toArray(), 'Address updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Address  $address
     * @return \Illuminate\Http\Response
     */
    public function destroy(Address $address)
    {
        $user_id = Auth::user()->id;
        if ( $address->user_id != $user_id ) {
            return $this->sendError('You are not authorized to delete this address', [], 403);
        }

        $address->delete();
        return $this->sendResponse([], 'Address deleted successfully.');
    }
}