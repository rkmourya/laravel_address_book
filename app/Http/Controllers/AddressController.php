<?php

namespace App\Http\Controllers;

use App\Address;
use App\User;
use Auth;
use Illuminate\Http\Request;
use Validator;

class AddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = Auth::user()->id;
        $address = Address::findMany($user_id);
        $addresses = User::with('address')->find($user_id)->address;
        return view('address.index',compact('addresses',$addresses));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('address.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validate
        $request->validate([
            'title' => 'required|min:3',
            'person_name' => 'required|min:3',
            'person_number' => 'required|min:10|numeric',
            'address_line1' => 'required|min:5',
            'address_line2' => 'min:3',
            'address_line3' => 'min:3',
            'pincode' => 'required|min:5',
            'city' => 'required|min:2',
            'state' => 'required|min:2',
            'country' => 'required|min:2'
        ]);

        $user_id = Auth::user()->id;

        #If use wants to set this as default address, clear already present default_type
        if ( !empty($request->default_type) ) {
            Address::clearDefault($user_id, $request->default_type);
        }

        $address = Address::create([
            'user_id' => $user_id,
            'title' => $request->title,
            'person_name' => $request->person_name,
            'person_number' => $request->person_number,
            'address_line1' => $request->address_line1,
            'address_line2' => $request->address_line2,
            'address_line3' => $request->address_line3,
            'pincode' => $request->pincode,
            'city' => $request->city,
            'state' => $request->state,
            'country' => $request->country,
            'default_type' => $request->default_type,
        ]);
        if( $address->exists ) {
            $request->session()->flash('message', 'Address saved.');
            $request->session()->flash('alert_class', 'alert-success');
        } else {
            $request->session()->flash('message', 'Error in saving address.');
            $request->session()->flash('alert_class', 'alert-danger');
        }
        return redirect('address');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Address  $address
     * @return \Illuminate\Http\Response
     */
    public function show(Address $address)
    {
        return abort(403, 'Access denied');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Address  $address
     * @return \Illuminate\Http\Response
     */
    public function edit(Address $address)
    {
        if ( $address->user_id != Auth::user()->id ) {
            return abort(403, 'Access denied');
        }

        return view('address.edit',compact('address',$address));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Address  $address
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Address $address)
    {
        $user_id = Auth::user()->id;
        if ( $address->user_id != $user_id ) {
            return abort(403, 'You are not authorized to update this address');
        }

        //Validate
        $request->validate([
            'title' => 'required|min:3',
            'person_name' => 'required|min:3',
            'person_number' => 'required|min:10|numeric',
            'address_line1' => 'required|min:5',
            'address_line2' => 'min:3',
            'address_line3' => 'min:3',
            'pincode' => 'required|min:5',
            'city' => 'required|min:2',
            'state' => 'required|min:2',
            'country' => 'required|min:2'
        ]);

        #If use wants to set this as default address, clear already present default_type
        if ( !empty($request->default_type) ) {
            Address::clearDefault($user_id, $request->default_type);
        }

        $address->title = $request->title;
        $address->person_name = $request->person_name;
        $address->person_number = $request->person_number;
        $address->address_line1 = $request->address_line1;
        $address->address_line2 = $request->address_line2;
        $address->address_line3 = $request->address_line3;
        $address->pincode = $request->pincode;
        $address->city = $request->city;
        $address->state = $request->state;
        $address->country = $request->country;
        $address->default_type = $request->default_type;
        $saved = $address->save();

        if ( !$saved ) {
            $request->session()->flash('message', 'Address update the failed!');
            $request->session()->flash('alert_class', 'alert-danger');
            return redirect('address/'.$address->id.'/edit');
        }

        $request->session()->flash('message', 'Successfully modified the address!');
        $request->session()->flash('alert_class', 'alert-success');
        return redirect('address');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Address  $address
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Address $address)
    {
        if ( $address->user_id != Auth::user()->id ) {
            return abort(403, 'Access denied');
        }

        $address->delete();
        $request->session()->flash('message', 'Successfully deleted the address!');
        $request->session()->flash('alert_class', 'alert-success');
        return redirect('address');
    }
}
