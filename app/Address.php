<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Address extends Model
{

    protected $fillable = [
        'user_id',
        'title',
        'person_name',
        'person_number',
        'address_line1',
        'address_line2',
        'address_line3',
        'pincode',
        'city',
        'state',
        'country',
        'default_type'
    ];

    public function user() {
        return $this->belongsTo('App\User');
    }

   public static function clearDefault($user_id, $default_type) {
       DB::table('addresses')
            ->where(['user_id'=>$user_id,'default_type'=>$default_type])
            ->update(['default_type' => null]);
   }
}
